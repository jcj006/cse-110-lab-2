package edu.ucsd.cs110s.temperature;

public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
      return Float.toString(value);
	}
	@Override
	public Temperature toCelsius() {
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
        return new Fahrenheit(value * 1.8f + 32);
	}
}
