package edu.ucsd.cs110s.temperature;

public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		return Float.toString(value);
	}
	@Override
	public Temperature toCelsius() {
		return new Celsius((value-32) * 5f/9f);
	}
	@Override
	public Temperature toFahrenheit() {
		return this;
	}
} 